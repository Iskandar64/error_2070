package mx.itesm.error_2070;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Daniel Alillo on 05/11/2017.
 */

public class Granada extends Objeto{

    private final float VELOCIDAD= 700;
    private final float angulo = (float)Math.toRadians(-30);
    private final float rotacion = (float)Math.toDegrees(angulo);

    private Animation<TextureRegion> spriteAnimado;
    private float timerAnimacion;

    public Granada(Texture textura, float x, float y){
        TextureRegion texturaCompleta = new TextureRegion(textura);
        // La divide en 2 frames de 32x64 (ver enemigo.png)
        TextureRegion[][] texturaBala = texturaCompleta.split(90,38);

        spriteAnimado = new Animation(0.5f, texturaBala[0][0], texturaBala[0][1], texturaBala[0][2]);
        // Animación infinita
        spriteAnimado.setPlayMode(Animation.PlayMode.LOOP);
        // Inicia el timer que contará tiempo para saber qué frame se dibuja
        timerAnimacion = 0;

        sprite = new Sprite(texturaBala[0][0]);
        sprite.setPosition(x,y);
    }


    public void dibujar(SpriteBatch batch, EstadoJuego estadoJuego){

        if(estadoJuego == EstadoJuego.JUGANDO){
            sprite.setRotation(rotacion);
            timerAnimacion += Gdx.graphics.getDeltaTime();
            // Frame que se dibujará
            TextureRegion region = spriteAnimado.getKeyFrame(timerAnimacion);
            batch.draw(region, sprite.getX(), sprite.getY(), sprite.getWidth()/2,sprite.getHeight()/2,
                    sprite.getWidth(),sprite.getHeight(),1,1,rotacion);
        }else{sprite.draw(batch);}
    }

    public void mover(float delta){
        sprite.setRotation(rotacion);
        sprite.setX(sprite.getX() + (float) Math.cos(angulo) * VELOCIDAD * delta);//Movimiento en X
        sprite.setY(sprite.getY() + (float) Math.sin(angulo) * VELOCIDAD * delta);//Movimiento en Y
    }

    public boolean chocaCon(Gorath gorath){
        return sprite.getBoundingRectangle().overlaps(gorath.sprite.getBoundingRectangle());
    }

    public boolean chocaCon(Nave nave){
        return sprite.getBoundingRectangle().overlaps(nave.sprite.getBoundingRectangle());
    }

    public boolean chocaCon(Barrera esudoDoom){
        return sprite.getBoundingRectangle().overlaps(esudoDoom.sprite.getBoundingRectangle());
    }

    public boolean chocaCon(Doom doom){
        return sprite.getBoundingRectangle().overlaps(doom.sprite.getBoundingRectangle());
    }

}
